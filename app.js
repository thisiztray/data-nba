const express = require('express');
const bodyParser = require('body-parser');
const nbaRoutes = require('./routes/nba');
const defRoutes = require('./routes/default');
const readme = require('readmeio');

const app = express();

app.use((req, res, next) => {
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

app.use(
    readme.metrics('iIgH6rZy7AySXECNN04CUl5Tn2XVHrIk', req => ({
      id: 0,
      label: 'Tray-test',
      email: 'aurelien.bauer@gmail.com',
    }), {
      development: true, // optional, sends logs to Development Data
    })
);

app.use(bodyParser.json());

app.use('/api', defRoutes);
app.use('/nba', nbaRoutes);

app.listen(3002);

module.exports = app;
