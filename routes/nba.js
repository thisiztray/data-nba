const express = require('express');
const router = express.Router();
const nbaCtrl = require('../controllers/nba');

router.get('/teams/:year', nbaCtrl.getTeams);
router.get('/teams/:year/:teamId', nbaCtrl.getTeamById);
router.get('/players/:year', nbaCtrl.getPlayers);
router.get('/players/:year/yearsPro/:yearsPro', nbaCtrl.getPlayers);
router.get('/players/:year/id/:playerId', nbaCtrl.getPlayer);
router.get('/schedule/:year', nbaCtrl.getSchedule);
router.get('/games/:date', nbaCtrl.getGames);
router.get('/boxscore/:date/:gamesFilter', nbaCtrl.getBoxscore);

module.exports = router;
