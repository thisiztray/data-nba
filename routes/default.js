const express = require('express');
const router = express.Router();
const defCtrl = require('../controllers/default');

router.get('/ping', defCtrl.ping);

module.exports = router;