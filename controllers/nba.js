const nba = require('nba.js').default;
const dateformat = require('dateformat');

exports.getTeams = (req, res, next) => {
    nba.data.teams({year: req.params.year})
        .then(result => {
            return res.status(200).json(result);
        })
        .catch(error => {
            console.error(error);
        });
};

exports.getTeamById = (req, res, next) => {
    nba.data.teams({year: req.params.year})
        .then(result => {
            for (team of result.league.standard) {
                if (team.teamId === req.params.teamId) {
                    return res.status(200).json(team);
                }
            }
        })
        .catch(error => {
            console.error(error);
        });
};

exports.getPlayers = async (req, res, next) => {
    let year = req.params.year;
    let playerlist = Promise;

    try {
        playerlist = await nba.data.players({year: year})
    } catch (e) {
        console.log(e);
        if (e.statusCode === 404) {
            return res.status(404).json({message: "Aucun joueurs correspondants à cette saison n'a été trouvé."});
        }
    }

    const players = [];
    const teams = await nba.data.teams({year: year});

    for (player of playerlist.league.standard) {
        if (player.teamId === '') {
            player.team = {fullName: '-'}
        } else {
            team = teams.league.standard.find(team => team.teamId === player.teamId);
            player.teamFullName = team.fullName;
        }
        if (req.params.yearsPro) {
            if (player.yearsPro !== '' && parseInt(player.yearsPro) <= req.params.yearsPro) {
                players.push(player);
            }
        } else {
            players.push(player);
        }
    }
    return res.status(200).json(players);
};

exports.getPlayer = async (req, res, next) => {
    let year = req.params.year;
    let playerlist = Promise;

    try {
        playerlist = await nba.data.players({year: year})
    } catch (e) {
        console.log(e);
        if (e.statusCode === 404) {
            return res.status(404).json({message: "Aucun joueur correspondant n'a été trouvé."});
        }
    }

    let playerTeamId = '';
    let playerProfile = {};
    for (player of playerlist.league.standard) {
        if (player.personId === req.params.playerId) {
            playerProfile = player;
            playerTeamId = player.teamId;
        }
    }
    let playerTeam = {};
    const teams = await nba.data.teams({year: year});
    playerTeam = teams.league.standard.find(team => team.teamId === playerTeamId);

    nba.data.teamSchedule({
        year: year,
        teamName: playerTeam.urlName
    })
        .then(result => {
            for (game of result.league.standard) {
                if (game.startDateEastern >= dateformat(new Date().toLocaleString("en-US", {timeZone: "America/New_York"}), 'yyyymmdd')) {
                    const player = {
                        profile: playerProfile,
                        team: playerTeam,
                        nextGame: game
                    };
                    return res.status(200).json(player);
                }
            }
        })
        .catch(error => console.log(error));
};

exports.getSchedule = (req, res, next) => {
    nba.data.schedule({year: req.params.year})
        .then(result => {
            return res.status(200).json(result);
        })
        .catch(error => {
            console.error(error);
            if (error.statusCode === 404) {
                return res.status(404).json({message: "Aucun calendrier correspondant à cette saison n'a été trouvé."})
            }
        });
};

exports.getBoxscore = async (req, res, next) => {
    let year = req.params.date.substring(0, 4);
    let schedule = Promise;

    try {
        schedule = await nba.data.schedule({year: year});

        const regex = new RegExp(req.params.date.toString());
        if (JSON.stringify(schedule).search(regex) === -1) {
            year = year - 1;
            schedule = await nba.data.schedule({year: year});
        }

    } catch (e) {
        console.log(e);
        if (e.statusCode === 404) {
            year = year - 1;
            schedule = await nba.data.schedule({year: year});
        }
    }

    const boxscore = [];
    for (game of schedule.league.standard) {
        if (game.startDateEastern !== req.params.date) {
            continue;
        }
        const bs = await nba.data.boxscore({
            date: req.params.date,
            gameId: game.gameId
        });
        boxscore.push(bs);
    }

    const playerlist = [];
    for (game of boxscore) {
        let vTeam = {};
        let hTeam = {};
        const teams = await nba.data.teams({year: year});
        if (game.basicGameData.period && game.basicGameData.period.current !== 0) {
            vTeam = teams.league.standard.find(team => team.teamId === game.basicGameData.vTeam.teamId);
            hTeam = teams.league.standard.find(team => team.teamId === game.basicGameData.hTeam.teamId);

            let timer = '';
            const clock = game.basicGameData.clock;
            const qt = game.basicGameData.period.current;
            const isHalftime = game.basicGameData.period.isHalftime;
            const isGameActivated = game.basicGameData.isGameActivated;

            if (isGameActivated) {
                if (!isHalftime) {
                    timer = clock === '' ? 'QT'+qt : 'QT'+qt+' - '+clock;
                } else {
                    timer = 'HT'
                }
            } else {
                if (req.params.gamesFilter === 'true') {
                    continue;
                }
                timer = 'Final';
            }

            for (player of game.stats.activePlayers) {
                const playerObj = {
                    id: player.personId,
                    name: player.firstName + ' ' + player.lastName,
                    team: player.teamId === vTeam.teamId ? vTeam : hTeam,
                    isOnCourt: player.isOnCourt,
                    clock: timer,
                    isGameActivated: isGameActivated,
                    score: getFantasyScore(player)
                };
                playerlist.push(playerObj);
            }
        }
    }
    playerlist.sort((a, b) => {
        return b.score - a.score;
    });
    let maxScore = Number;
    if (Object.keys(playerlist).length !== 0 && req.params.gamesFilter !== 'true') {
        const max = playerlist.reduce((a, b) => a.score > b.score ? a : b);
        maxScore = max.score;
    }

    return res.status(200).json({playerlist, maxScore});
};

exports.getGames = async (req, res, next) => {
    let year = req.params.date.substring(0, 4);
    let schedule = Promise;

    try {
        schedule = await nba.data.schedule({year: year});

        const regex = new RegExp(req.params.date.toString());
        if (JSON.stringify(schedule).search(regex) === -1) {
            year = year - 1;
            schedule = await nba.data.schedule({year: year});
        }

    } catch (e) {
        console.log(e);
        if (e.statusCode === 404) {
            year = year - 1;
            schedule = await nba.data.schedule({year: year});
        }
    }

    const games = [];
    for (game of schedule.league.standard) {
        if (game.startDateEastern !== req.params.date) {
            continue;
        }
        const bs = await nba.data.boxscore({
            date: req.params.date,
            gameId: game.gameId
        });
        games.push(bs);
    }

    return res.status(200).json(games);
};

function getFantasyScore(player) {
    const bonus = Number(player.points) + Number(player.fgm) + Number(player.ftm) + Number(player.tpm)
        + Number(player.totReb) + Number(player.assists) + Number(player.steals) + Number(player.blocks);

    const malus = (Number(player.fga) - Number(player.fgm)) + (Number(player.tpa) - Number(player.tpm))
        + (Number(player.fta) - Number(player.ftm)) + Number(player.turnovers);

    return Number(bonus) - Number(malus);
}
